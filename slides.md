---
theme: academic
layout: cover
# coverAuthor: William Xu, Jiawen Zhao, Krishnaveni Parvataneni, Vincent Zhang, Sophia Lin, Hongning Wang
background: https://source.unsplash.com/collection/94734566/1920x1080
class: 'text-center'
colorSchema: 'light'
highlighter: shiki
lineNumbers: false
info: |
  ## Slides for PHM-Yantai-2022: Paper 254
---

# Hybrid AI Framework for Remote Patient Heart Failure Risk Prediction

Placeholder, won't be used.

---
layout: center
---

# Related Work

---
layout: default
---

## Review of Existing AI Research

There has been many research proposals attempting to predict heart failure using machine learning models. The following list some research advancements.

<table class="table-auto text-[9px]"><thead><tr><td>Paper</td><td>Purpose</td><td>Methodologies</td><td>Year</td></tr></thead><tbody><tr><td>Arrhythmic sudden death survival prediction using deep learning analysis of scarring in the heart</td><td>Predict survival curves and heart disease risk for a range of 10 years.</td><td>Used a novel deep learning approach that blends neural networks and survival analysis.</td><td>2022</td></tr></tbody><tbody><tr><td>Cardiovascular disease screening in women: leveraging artificial intelligence and digital tools</td><td>Predict cardiovascular risk for women.</td><td>Used regression and Convolutional Neural Network (CNN) on multiple demographic factors.</td><td>2022</td></tr></tbody><tbody><tr><td>Remote monitoring in heart failure: current and emerging technologies in the context of the pandemic</td><td>Using a multiparameter model with both patient’s self-collected data and data from hospital-grade machinery.</td><td>Used a multiparameter model based on vital signs, lung congestion, hemodynamics.</td><td>2022</td></tr></tbody><tbody><tr><td>Representation learning in intraoperative vital signs for heart failure risk prediction</td><td>Predict heart risk for patients during and after operations.</td><td>Used CNN on statistical, text-based and image representations of intraoperative patient data.</td><td>2019</td></tr></tbody><tbody><tr><td>Predictive models for risk assessment of worsening events in chronic heart failure patients</td><td>Predict heart risk in patients with chronic heart failure.</td><td>Used a Linear Support Vector Machine to analyze personal data, vital and clinical parameters, and cardiovascular worsening events between visits.</td><td>2018</td></tr></tbody><tbody><tr><td>Analysis of machine learning techniques for heart failure readmissions</td><td>Predict readmissions in discharged heart failure patients.</td><td>Used random forests, boosting, support vector machines, and logistic regression to analyze intraoperative data.</td><td>2016</td></tr></tbody><tbody><tr><td>Remote health monitoring: predicting outcome success based on contextual features for cardiovascular disease</td><td>Identify patients' key baseline contextual features and build effective prediction models that help determine remote health monitoring outcome success.</td><td>Used the Wanda-CVD smartphone-based monitoring system to measure blood pressure, BMI, LDL, HDL cholesterol and detect heart risk.</td><td>2014</td></tr></tbody></table>

<!-- 
As we can see, there is a great diversity of models applicable to predict cardiovascular diseases. Paper 7 uses Convolutional Neural Network to analyze cardiovascular disease for women. Paper 10 uses Linear Support-Vector Machines to create risk assessments for chronic heart failure patients. Paper 11 utilizes multiple models, including but not limited to random forst, support-vector machines, and logistic regressions, to predict heart failure readmissions. 

Additionally, the papers shown also use a wide variety of input features. This includes physical activity (such as daily steps), vital signs such as cholesterol levels, blood pressure, demographic information, and non-cardiovascular medical conditions such as lung congestions.
 -->

---
layout: default
---

## Clinical Practices

Hospitals have also adopted various measures to monitor their heart-failure patients. However, they all have their own shortcomings.

1. Post-discharge follow-ups. This can be in the form of phone calls or visits to the clinic.
    - Requires additional human resources.
    - Not practical for all patients.
2. Holter Monitors
    - Influenced by many factors.
    - Sometimes unreliable: some studies show that they catch only around 47% of arrhythmias.
    - Requires manual analysis of data.
    - Cost: $3000 with additional cost of electrodes.

<!-- 
Here are a few popular ones:

- Post-discharge follow-ups.
    - Can be inefficient, since a hospital can only handle a fixed number of patients at one time.
    - Moreover, for patients that live far away or have severe physical disabilities, coming to the hospital in person can be extremely difficult or impossible.
- Holter monitors is a type of portable electrocardiogram with multiple electrodes that can record the heart’s electrical activity for over 24 hours.
    - conditions like smoking, sweating, going near strong electric fields, or taking medications can all affect the accuracy
    - are shown to catch only around 47% of arrhythmias, making them a less reliable device
    - No underlying prediction models so their data must be analyzed by medical professionals, which makes them less efficient and very likely to be too late to alert the patient.
    - Cost (use slides)
 -->

---
layout: center
---

# Proposed Approach

---
layout: two-cols-header
---

## C. Personalized Benchmark of Circadian Rhythm

Circadian rhythm is the biological cycle that controls both the mental and physical activity cycle of humans over the span of 24 hours.

We utilized the K-Means clustering model to group active and passive time ranges. The objective of K-Means clustering is to minimize each point’s Euclidean distance in the cluster from a centroid.

<!-- ::left:: -->

$$
WCSS \text{ (Within-Cluster Sum-of-Squares)} = \sum^{n}_{i = 0}{\min_{1 \le j \le k}{|S_j - c_j|^2}}
$$

<!-- ::right:: -->

<v-click>

<center>
<img src="/wcss-vs-k.jpg" class="h-50 rounded shadow" />
</center>

</v-click>

<!-- 
Our research analyzed time series-based activity features representing activity intensity, duration, and time of the day. The model allows us to find irregularities in ill subjects compared to the personalized daily benchmark.

We first must determine the optimal cluster count. [continue on with objective] Therefore, we can obtain the loss function associated with each cluster, which we call WCSS (the Within-Cluster Sum-of-Squares). The figure below shows the relationship between the loss function and the cluster count (k-value) when plotted for a patient’s activity data.
 -->

---
layout: default
---

To find the best k-value based on our data, we need to know where the inertia function reaches maximum curvature (vertex).

<v-click>

In formal terms, if the inertia function is represented by $I(k)$, then we see
$$
\underset{k \in \mathbb{N} | k > 0}{\arg\min} (\frac{|I''(k)|}{\sqrt{(1 + I'(k)^2)^3}}) = 2
$$

</v-click>

<v-click>

In other words, the optimal k-value where $I(k)$ reaches maximum curvature is 2. Figure below is a plot of k-means being applied to the raw data of one patient.

<center>
<img src="/raw-cr.jpg" class="h-50" />
</center>

</v-click>

<!-- 
[for the equation] where the I `prime` k, and I `double prime` k are the first and second derivatives of I(k).

Equation pronounciation: argmin for all positive integer $k$ of [the fraction].
 -->

---
default
---

However, the raw data is very disorderly showing the overlap of wake and sleep time. Also, subtle activities at night shouldn't constitute the patient's circadian rhythm.

**Rolling-mean**: if $h$ is the current hour and $\{A_h\}^{23}_{h=0}$ is the set of all activities for every 24 hours, then
<v-click>

$$
\mu_h = \frac{A_{h-1} + A_h + A_{h + 1}}{3}
$$

</v-click>

<v-click>

After averaging the values for all three days and applying a rolling mean for every 3 hours, the plot of the same patient looks like this:
<center>
<img src="/rolled-cr.jpg" class="h-50 rounded shadow" />
</center>

</v-click>

<!-- 
For instance, the patient may decide to wake up, use the restroom, then go back to sleep. We cannot count this as the awake portion of a patient’s circadian rhythm.

To fix the issues, we take the average of these three days to reduce the number of data points scattered around.

Most importantly, we use a rolling-mean of 3 hours to even out the anomalies.

The curve now looks much more pronounced and the boundary in circadian rhythm looks much more distinct. A pattern can even be noticed.
This patient appears to have woken up at 8:00 and fell back to sleep around 19:00, achieving peak physical activity around 15:00.
 -->

---
layout: two-cols-header
---

## D. Rule-Based Algorithm

We also analyzed correlation between heart rate and activities to understand the heart health condition of a user.

::left::

For users with potential heart failure risk, the correlation between heart rate and activities is relatively low, with a coefficient of determination less than 0.1.

<FigureWithCaption caption="Users with heart failure risk experienced low correlation between heart rate and activities" url="https://gitlab.com/GZGavinZhao/hafrphfrp/-/raw/main/public/hf-rule-based.jpg?inline=false" />

::right::

For healthy users, the correlation is quite high. The R-squared value was incorporated in our rule-based algorithm.

<FigureWithCaption caption="Healthy users experience relatively high correlation between heart rate and activities" url="https://gitlab.com/GZGavinZhao/hafrphfrp/-/raw/main/public/healthy-rule-based.jpg?inline=false" />

<!-- 
Annotations!
 -->
