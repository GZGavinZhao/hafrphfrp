import 'package:grinder/grinder.dart';
import 'package:universal_html/html.dart';

main(args) => grind(args);

const List<String> cats = [
  'Paper',
  'Purpose',
  'Methodologies',
  // 'Limitations',
  'Year',
];
const List<List<String>> works = [
  [
    'Arrhythmic sudden death survival prediction using deep learning analysis of scarring in the heart',
    'Predict survival curves and heart disease risk for a range of 10 years.',
    'Used a novel deep learning approach that blends neural networks and survival analysis.',
    '2022',
  ],
  [
    'Cardiovascular disease screening in women: leveraging artificial intelligence and digital tools',
    'Predict cardiovascular risk for women.',
    'Used regression and Convolutional Neural Network (CNN) on multiple demographic factors.',
    '2022',
  ],
  [
    "Remote monitoring in heart failure: current and emerging technologies in the context of the pandemic",
    "Using a multiparameter model with both patient’s self-collected data and data from hospital-grade machinery.",
    "Used a multiparameter model based on vital signs, lung congestion, hemodynamics.",
    "2022",
  ],
  [
    "Representation learning in intraoperative vital signs for heart failure risk prediction",
    "Predict heart risk for patients during and after operations.",
    "Used CNN on statistical, text-based and image representations of intraoperative patient data.",
    "2019",
  ],
  [
    "Predictive models for risk assessment of worsening events in chronic heart failure patients",
    "Predict heart risk in patients with chronic heart failure.",
    "Used a Linear Support Vector Machine to analyze personal data, vital and clinical parameters, and cardiovascular worsening events between visits.",
    "2018",
  ],
  [
    "Analysis of machine learning techniques for heart failure readmissions",
    "Predict readmissions in discharged heart failure patients.",
    "Used random forests, boosting, support vector machines, and logistic regression to analyze intraoperative data.",
    "2016",
  ],
  [
    "Remote health monitoring: predicting outcome success based on contextual features for cardiovascular disease",
    "Identify patients' key baseline contextual features and build effective prediction models that help determine remote health monitoring outcome success.",
    "Used the Wanda-CVD smartphone-based monitoring system to measure blood pressure, BMI, LDL, HDL cholesterol and detect heart risk.",
    "2014",
  ]
];

void buildCsv() {}

@Task()
void html() {
  final table = TableElement();
  table.classes = ["table-auto", "text-xs"];

  final head = table.createTHead();
  final hRow = head.addRow();
  for (var i = 0; i < cats.length; i++) {
    final cell = hRow.insertCell(i);
    cell.innerText = cats[i];
  }

  for (var i = 0; i < works.length; i++) {
    final row = table.addRow();
    for (var j = 0; j < works[i].length; j++) {
      // Bruh, row.addCell() should work...
      final td = row.insertCell(j);
      td.innerText = works[i][j];
    }
  }

  print(table.outerHtml);
}

@Task()
clean() => defaultClean();
